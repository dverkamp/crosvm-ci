#!/bin/bash
# Copyright 2020 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -ex
cd "${0%/*}"


if [[ ! -d crosvm ]]; then
  git clone --depth=1 https://chromium.googlesource.com/chromiumos/platform/crosvm
fi

published_url="https://crosvm-ci.gitlab.io/crosvm-ci/"
current_commit=$(git -C crosvm rev-parse HEAD)
published_commit=$(curl -sf "${published_url}commit")

if [[ "${published_commit}" = "${current_commit}" ]]; then
  exit
fi

mkdir -p build

./crosvm/docker/build_crosvm_base.sh

docker run --rm \
    -v "${PWD}"/crosvm:/platform/crosvm \
    -v "${PWD}"/build:/build \
    crosvm-base \
    /bin/bash -c \
    "cargo doc --all-features --target-dir /build && chown -R $(id -u) /build"

mv build/doc public

# Redirect the index to the crosvm module doc.
cat >public/index.html <<INDEX
<script>location="crosvm"</script>
INDEX

# Include commit info of this published doc.
echo -n "${current_commit}" >public/commit

